// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import UserProfile from "components/Customer/UserProfile.js";
import TableList from "components/TableList/TableList.js";
import CustomerIdentification from "components/Customer/CustomerIdentification.js";
import CustomerList from "components/Customer/CustomerList.js";
import Payment from "components/Payment/Payment.js";

const dashboardRoutes = [
  {
    path: "/CustomerList",
    name: "List of Customer",
    icon: LibraryBooks,
    component: CustomerList,
    layout: "/admin"
  },
  // {
  //   path: "/cl",
  //   name: "List of Customer",
  //   icon: LibraryBooks,
  //   component: CL,
  //   layout: "/admin"
  // },
  // {
  //   path: "/CustomerEdit",
  //   name: "Edit Customer",
  //   icon: LibraryBooks,
  //   component: CustomerEdit,
  //   layout: "/admin"
  // },
  {
    path: "/user",
    name: "Customer Profile",
    icon: Person,
    component: UserProfile,
    layout: "/admin"
  },
  {
    path: "/customeridentification",
    name: "Customer Identification",
    icon: Dashboard,
    component: CustomerIdentification,
    layout: "/admin"
  },
    {
    path: "/table",
    name: "Invoice",
    icon: "content_paste",
    component: TableList,
    layout: "/admin"
  },
  {
    path: "/payment",
    name: "Payment",
    icon: LibraryBooks,
    component: Payment,
    layout: "/admin"
  },
  // {
  //   path: "/Createemployee",
  //   name: "Payment",
  //   icon: LibraryBooks,
  //   component: Createemployee,
  //   layout: "/admin"
  // },
  // {
  //   path: "/edit/:id",
  //   name: "Payment",
  //   icon: LibraryBooks,
  //   component: Editemployee,
  //   layout: "/admin"
  // },
  // {
  //   path: "/EmployeList",
  //   name: "Payment",
  //   icon: LibraryBooks,
  //   component: EmployeList,
  //   layout: "/admin"
  // },
 
  // {
  //   path: "/upgrade",
  //   name: "Upgrade",
  //   icon: LibraryBooks,
  //   component: UpgradeToPro,
  //   layout: "/admin"
  // },
  // {
  //   path: "/icons",
  //   name: "Icons",
  //   rtlName: "الرموز",
  //   icon: BubbleChart,
  //   component: Icons,
  //   layout: "/admin"
  // },
  // {
  //   path: "/maps",
  //   name: "Maps",
  //   rtlName: "خرائط",
  //   icon: LocationOn,
  //   component: Maps,
  //   layout: "/admin"
  // },
  // {
  //   path: "/notifications",
  //   name: "Notifications",
  //   rtlName: "إخطارات",
  //   icon: Notifications,
  //   component: NotificationsPage,
  //   layout: "/admin"
  // },

];

export default dashboardRoutes;
