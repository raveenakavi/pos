
import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import routes from "layouts/routes.js"

// core components
import Admin from "layouts/Admin.js";

import "assets/css/material-dashboard-react.css?v=1.9.0";
import Login from "components/Dashboard/login";
import Privateroute from "components/Dashboard/protect";
const hist = createBrowserHistory();

ReactDOM.render(
  <Router history={hist}>
    <Switch>
      <Privateroute path="/admin" component={Admin} />
      <Route path="/" component={Login} />
      <Privateroute path="/routes" component={routes} />
      <Privateroute  to="/admin/dashboard" />
    </Switch>
  </Router>,
  document.getElementById("root")
);
