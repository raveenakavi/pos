import React, { useEffect, useState } from 'react';
// @material-ui/core common
import { makeStyles } from "@material-ui/core/styles";
// core common
import GridItem from "common/Grid/GridItem.js";
import GridContainer from "common/Grid/GridContainer.js";
import Button from "common/CustomButtons/Button.js";
import Card from "common/Card/Card.js";
import CardHeader from "common/Card/CardHeader.js";
import CardBody from "common/Card/CardBody.js";
import CardFooter from "common/Card/CardFooter.js";
import axios from 'axios';
import {useHistory} from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "500",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  },
  GridItem: {
    margin: "20px",
  },
  
};
const useStyles = makeStyles(styles);
const getpostcode = async ()=>{
  await axios.get('https://api.postalpincode.in/pincode/622301').then((res)=>{
    console.log(res.data[0].PostOffice);
  });
}

export default function UserProfile() {
  const history = useHistory();
    
    const intialize = {
        username : "",
        membership: "",
        email : "",
        password : "",
        contact_number : "",
        pincode : "",
        address : "",
        pan : "",
        gststate:"",
        gsstin : "",
        gstname : "",
        gst_registername : "",
        error:""
    }
    
    const [customer, setcustomer] = useState(intialize);
    const [district, setdistrict] = useState([]);
    const [statename, setstatename] = useState([]);
    //const [town, settown]  = useState([]);
    const [dob, setdob] = useState("");
    const [doa, setdoa] = useState("");
    const [error, seterror] = useState();
    const [username, setusername] = useState();
    const [email, setemail] = useState();
    const {membership,contact_number,address,pan,gststate,gsstin,gst_registername,pincode} = customer;
        
    const getpostcode = async (pincodedata)=>{
        await axios.get('https://api.postalpincode.in/pincode/'+pincodedata).
                then((res)=>{
                    const postoffice = res.data[0].PostOffice;
                    setdistrict(postoffice[0].District);
                    setstatename(postoffice[0].State);
                    //settown(postoffice);
                }).catch((err)=>{
                    console.log("pincode not found");
                });
    }
    
    const onInputChange =  (e)=>{
        const ename = e.target.name;
        const evalue = e.target.value;
        setcustomer({...customer,[ename] : evalue});
        if(ename == "pincode" && evalue.length == 6){
           getpostcode(evalue);
          
        }else{
            setdistrict("");
            setstatename("");
        }
        
    }
    
    const onSubmit = async(e)=>{
        e.preventDefault();
        var customerdetails = {"statename":statename,"district":district,"dob":dob.toString(),"doa":doa.toString()};
        console.log(customerdetails);
        var result = {...customer,...customerdetails};
        console.log(result)
        if (!result.username) {
         setusername(!result.username)
        }else if (!result.email) {
          setemail()
         }else{
            await axios.post("http://localhost:5001/customer/add",result).then((res)=>{
                  if(res){
                    history.push('admin/Dashboard');
                    }
                }).catch((err)=>{
                    console.log("err")
                });
        }
       
    }

    // const onSubmit = async(e)=>{
    //     e.preventDefault();
    //     await axios.post("http://localhost:5001/customer/add",customer).then((res)=>{
    //        // if(res){
    //             history.push('admin/dashboard')
    //         //}
           
    //     }).catch((err)=>{
    //        // console.log("err")  
    //     });
    // }

  const [divShow, setDivShow] = useState(false);
  const selectChange = (element) =>{
    if(element == 0){
      setDivShow(false);
    }else if(element==1){
      setDivShow(true);
    }
  }
   const classes = useStyles();
   
  return (
    <div>
      <form onSubmit={e=>onSubmit(e)}>
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Customer Profile Creation</h4>
              <p className={classes.cardCategoryWhite}>Complete your profile with Required Field</p>
            </CardHeader>
            <CardBody>
              <GridContainer>
                {/* <GridItem xs={12} sm={12} md={5}>
                  <CustomInput
                    labelText="Company (disabled)"
                    id="company-disabled"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      disabled: true
                    }}
                  />
                </GridItem> */}
                <GridContainer xs={12} sm={12} md={12}>
                <GridItem xs={3} sm={3} md={2}>
                  <label>Name :</label>
                  </GridItem>
                  <GridItem xs={3} sm={3} md={4}>
                  <input
                    labelText="Name"
                    id="name"
                    formControlProps={{
                      fullWidth: true
                    }}
                    name="username" onChange={e=>onInputChange(e)} value={username}
                  />
                  {(username)? (<>
                    <p className="help is-danger">username not exists</p>
                  </>):null}
                 
                </GridItem>
                <GridItem xs={3} sm={3} md={2}>
                  <label>Membership No.</label>
                  </GridItem>
                  <GridItem xs={3} sm={3} md={4}>
                  <input
                    labelText="membership Number"
                    id="membership"
                    formControlProps={{
                      fullWidth: true
                    }}
                    name="membership" onChange={e=>onInputChange(e)} value={membership}
                  />
                </GridItem>
                
                <GridItem xs={3} sm={3} md={2}>
                  <label>Email :</label>
                  </GridItem>
                  <GridItem xs={3} sm={3} md={4}>
                  <input
                    labelText="Email address"
                    id="email-address"
                    formControlProps={{
                      fullWidth: true
                    }}
                    name="email" onChange={e=>onInputChange(e)} value={email}
                  />
                  {(email)? (<>
                    <p className="help is-danger">Email not exists</p>
                  </>):null}
                </GridItem>

                <GridItem xs={3} sm={3} md={2}>
                  <label>Contact Number : </label>
                  </GridItem>
                  <GridItem xs={3} sm={3} md={4}>
                  <input
                  type="number"
                    labelText="Contact Number"
                    id="contact_number"
                    name="contact_number" onChange={e=>onInputChange(e)} value={contact_number}                    formControlProps={{
                      fullWidth: true
                    }}  
                  />
                </GridItem>
                <GridItem xs={3} sm={3} md={2}>
                  <label>Alternative Number : </label>
                  </GridItem>
                  <GridItem xs={3} sm={3} md={4}>
                  <input
                  type="number"
                    labelText="Contact Number"
                    id="contact_number"
                    name="contact_number" onChange={e=>onInputChange(e)} value={contact_number}                    formControlProps={{
                      fullWidth: true
                    }}  
                  />
                </GridItem>

                <GridItem xs={3} sm={3} md={2}>
                  <label>Gender: </label>
                  </GridItem>
                  <GridItem xs={3} sm={3} md={4}>
                  <label>Male</label> <input type="radio" title="gender" name="something[1]" value="Male" class="inp" />
                  <label>Female</label><input type="radio" title="gender" name="something[1]" value="Female" class="inp" />
                </GridItem>

                <GridItem xs={3} sm={3} md={2}>
                    <label>Pincode</label>
                    </GridItem>
                    <GridItem xs={3} sm={3} md={4}>
                    <input type="number" minLength={6} maxLength={6} className="form-control" name="pincode"  onChange={e=>onInputChange(e)} value={pincode}  />
                </GridItem>
                <GridItem xs={3} sm={3} md={2}>
                    <label>District</label>
                    </GridItem>
                    <GridItem xs={3} sm={3} md={4}>
                    <input type="text" className="form-control" name="district" value={district} />
                </GridItem>
                <GridItem xs={3} sm={3} md={2}>
                    <label>State</label>
                    </GridItem>
                    <GridItem xs={3} sm={3} md={4}>
                    <input type="text" className="form-control" name="state" value={statename} />
                </GridItem>             
                <GridItem xs={3} sm={3} md={2}>
                  <label>Address : </label>
                  </GridItem>
                  <GridItem xs={3} sm={3} md={4}>
                  <input
                  type="textarea"
                    labelText="Address"
                    id="address"
                    name="address" onChange={e=>onInputChange(e)} value={address}
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      multiline: true,
                      rows: 5
                    }}
                  />
                </GridItem>
                <GridItem xs={3} sm={3} md={2}>
                  <label>DOB : </label>
                  </GridItem>
                  <GridItem xs={3} sm={3} md={4}>
                  <DatePicker class="form-control" selected={dob} onChange={e => setdob(e)} />

  {/* <input
    id="dob"
    label="DOB"
    type="date"
    name="dob" onChange={e=>onInputChange(e)} value={dob}
    defaultValue="yyyy-mm-dd"
    className={classes.textField}
    InputLabelProps={{
      shrink: true,
    }}
  /> */}
                </GridItem>
                <GridItem xs={3} sm={3} md={2}>
                  <label>DOA : </label>
                  </GridItem>
                  <GridItem xs={3} sm={3} md={4}>
                 {/* <input
    id="doa"
    label="DOA"
    type="date"
    name="doa" onChange={e=>onInputChange(e)} value={doa}
    defaultValue="yyyy-mm-dd"
    className={classes.textField}
    InputLabelProps={{
      shrink: true,
    }}
  /> */}
                      <DatePicker class="form-control" selected={doa} onChange={date => setdoa(date)} />

                </GridItem>  

                <GridItem xs={3} sm={3} md={2}>
                  <label>PAN : </label>
                  </GridItem>
                  <GridItem xs={3} sm={3} md={4}>
                  <input
                    labelText="PAN"
                    id="pan"
                    name="pan" onChange={e=>onInputChange(e)} value={pan}
                    
                    formControlProps={{
                      fullWidth: true
                    }}
                  />
                  </GridItem>
                  <GridItem xs={3} sm={3} md={2}>
                  <label>GSTIN : </label>
                  </GridItem>
                  <GridItem xs={3} sm={3} md={4}>
                 <select id="sel_box" onChange={(e)=>selectChange(e.target.value)}>
  <option value="0">No</option>
  <option value="1">Yes</option>
</select>
                </GridItem>

                {(divShow)?(<>
                  <GridContainer id="date_div">
                  <GridItem xs={3} sm={3} md={2}>
                  <label>GSTIN <br />(to be validated through API)</label>
                  </GridItem>
                  <GridItem xs={3} sm={3} md={4}>
                  <input
                    labelText="GSTIN (to be validated through API)"
                    id="gststate"
                    name="gststate" onChange={e=>onInputChange(e)} value={gststate}
                    formControlProps={{
                      fullWidth: true
                    }}
                  />
                </GridItem>
                <GridItem xs={3} sm={3} md={2}>
                  <label>Type of Registration</label>
                  </GridItem>
                  <GridItem xs={3} sm={3} md={4}>
                  <input
                    labelText="Type of Registration"
                    id="gsstin"
                    name="gsstin" onChange={e=>onInputChange(e)} value={gsstin}
                    formControlProps={{
                      fullWidth: true
                    }}
                  />
                </GridItem>
                <GridItem xs={3} sm={3} md={2}>
                  <label>Trade/Registered <br />Name as per GSTIN</label>
                  </GridItem>
                  <GridItem xs={3} sm={3} md={4}>
                  <input
                    labelText="Trade/Registered Name as per GSTIN"
                    id="trade-registration"
                    name="gst_registername" onChange={e=>onInputChange(e)} value={gst_registername}
                    formControlProps={{
                      fullWidth: true
                    }}
                  />
                  </GridItem> 
                  </GridContainer>
                </>):null}
                  </GridContainer>
                </GridContainer>
              <GridContainer>

                {/* <Pincode /> */}
              {/* <GridItem xs={12} sm={12} md={6}><p><label>Zip Code:</label> <input type="text" id="zip" /></p></GridItem>
              <GridItem xs={12} sm={12} md={6}><p><label>City: </label><input type="text" id="city" /></p></GridItem>
              <GridItem xs={12} sm={12} md={6}><p><label>State: </label><input type="text" id="state" /></p></GridItem>
              <GridItem xs={12} sm={12} md={6}><p><label>Village/Town: </label><input type="text" id="state" /></p></GridItem> */}
              </GridContainer>
              <GridContainer>



                {/* <GridItem xs={12} sm={12} md={4}>
                  <label>GSTIN : </label>
                  </GridItem>
                  <GridItem xs={12} sm={12} md={8}>
                 <select id="sel_box" onChange={(e)=>selectChange(e.target.value)}>
  <option value="0">No</option>
  <option value="1">Yes</option>
</select>
                </GridItem>


                {(divShow)?(<>
                  <GridContainer id="date_div">
                  <GridItem xs={12} sm={12} md={4}>
                  <label>GSTIN <br />(to be validated through API)</label>
                  </GridItem>
                  <GridItem xs={12} sm={12} md={8}>
                  <input
                    labelText="GSTIN (to be validated through API)"
                    id="gststate"
                    name="gststate" onChange={e=>onInputChange(e)} value={gststate}
                    formControlProps={{
                      fullWidth: true
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <label>Type of Registration</label>
                  </GridItem>
                  <GridItem xs={12} sm={12} md={8}>
                  <input
                    labelText="Type of Registration"
                    id="gsstin"
                    name="gsstin" onChange={e=>onInputChange(e)} value={gsstin}
                    formControlProps={{
                      fullWidth: true
                    }}
                  />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <label>Trade/Registered <br />Name as per GSTIN</label>
                  </GridItem>
                  <GridItem xs={12} sm={12} md={8}>
                  <input
                    labelText="Trade/Registered Name as per GSTIN"
                    id="trade-registration"
                    name="gst_registername" onChange={e=>onInputChange(e)} value={gst_registername}
                    formControlProps={{
                      fullWidth: true
                    }}
                  />
                  </GridItem> */}
                  {/* </GridContainer>
                </>):null} */}

              </GridContainer>
              {/* <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <InputLabel style={{ color: "#AAAAAA" }}>About me</InputLabel>
                  <CustomInput
                    labelText="Lamborghini Mercy, Your chick she so thirsty, I'm in that two seat Lambo."
                    id="about-me"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      multiline: true,
                      rows: 5
                    }}
                  />
                </GridItem>
              </GridContainer> */}
            </CardBody>
            <CardFooter>
              <Button type="submit" color="primary">Submit</Button>
            </CardFooter>
          </Card>
        </GridItem>
        {/* <GridItem xs={12} sm={12} md={4}>
          <Card profile>
            
            <CardBody profile>
              <h6 className={classes.cardCategory}>Mebership No.(to be auto generated)</h6>
              <h4 className={classes.cardTitle}>1111111</h4>
                          </CardBody>
          </Card>
        </GridItem> */}
        {/* <GridItem xs={12} sm={12} md={4}>
          <Card profile>
            <CardAvatar profile>
              <a href="#pablo" onClick={e => e.preventDefault()}>
                <img src={avatar} alt="..." />
              </a>
            </CardAvatar>
            <CardBody profile>
              <h6 className={classes.cardCategory}>CEO / CO-FOUNDER</h6>
              <h4 className={classes.cardTitle}>Alec Thompson</h4>
              <p className={classes.description}>
                Don{"'"}t be scared of the truth because we need to restart the
                human foundation in truth And I love you like Kanye loves Kanye
                I love Rick Owens’ bed design but the back is...
              </p>
              <Button color="primary" round>
                Follow
              </Button>
            </CardBody>
          </Card>
        </GridItem> */}
      </GridContainer>
      </form>
    </div>
  );
}
