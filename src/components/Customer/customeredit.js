import React, { useEffect, useState } from 'react';
import axios from 'axios';
import {useHistory, useParams, Link} from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Moment from 'moment';

const Customeredit = ()=>{
    const history = useHistory();
    const {id} = useParams();
    
    const intialize = {
        username : "",
        email : "",
        password : "",
        contact_number : "",
        pincode : "",
        address : "",
        pan : "",
        gstin : "",
        gstname : ""
    }
    
    const [customer, setcustomer] = useState(intialize);
    const [district, setdistrict] = useState([]);
    const [statename, setstatename] = useState([]);
    //const [town, settown]  = useState([]);
    const [dob, setdob] = useState("");
    const [doa, setdoa] = useState("");
    
    const {username,email,password,contact_number,address,pan,gstin,gstname,pincode} = customer;
        
    const getpostcode = async (pincodedata)=>{
        await axios.get('https://api.postalpincode.in/pincode/'+pincodedata).
                then((res)=>{
                    const postoffice = res.data[0].PostOffice;
                    setdistrict(postoffice[0].District);
                    setstatename(postoffice[0].State);
              
                }).catch((err)=>{
                    console.log("pincode not found");
                });
    }
    
    const onInputChange =  (e)=>{
        const ename = e.target.name;
        const evalue = e.target.value;
        setcustomer({...customer,[ename] : evalue});
        if(ename == "pincode" && evalue.length == 6){
           getpostcode(evalue);
          
        }
    }

    const editcustomer = async()=>{
        const result = await axios.get(`http://localhost:5001/Customer/${id}`);
        setdistrict(result.data.district)
        setstatename(result.data.statename);
        setcustomer(result.data);
        
        setdob(Moment(result.data.dob).format('MM/DD/YYYY'));
        setdoa(Moment(result.data.doa).format('MM/DD/YYYY')); 
        }
    useEffect(()=>{
        editcustomer();
    },[]);
    
    const onSubmit = async(e)=>{
        e.preventDefault();
        
        var customerdetails = {"statename":statename,"district":district,"dob":dob.toString(),"doa":doa.toString()};
        console.log(customerdetails)
        var result = {...customer,...customerdetails};
        console.log(`http://localhost:5001/customer/${id}`)
        await axios.put(`http://localhost:5001/customer/update/${id}`,result).then((res)=>{
           if(res){
                history.push('/admin/dashboard');
            }
        }).catch((err)=>{
                history.push('/admin/CustomerEdit');
        });
    }
   
    return(
        <div className="container">
             <div className="row">
                <div class="col-lg-12">
                    <h1>Customer Details</h1>
                    <Link className="btn btn-primary float-right" to="/admin/dashboard">Back</Link>
                </div>
            </div>
            <form onSubmit={e=>onSubmit(e)}>
                <div className="form-group">
                    <label>UserName</label>
                    <input type="text" className="form-control" name="username" onChange={e=>onInputChange(e)} value={username} />
                </div>
                <div className="form-group">
                    <label>Email address</label>
                    <input type="email" className="form-control" name="email" onChange={e=>onInputChange(e)} value={email} />
                </div>
                <div className="form-group">
                    <label>Contact Number</label>
                    <input type="text" className="form-control" name="contact_number" onChange={e=>onInputChange(e)} value={contact_number} />
                </div>
                <div className="form-group">
                    <label>Pincode</label>
                    <input type="number" minLength={6} maxLength={6} className="form-control" name="pincode"  onChange={e=>onInputChange(e)} value={pincode}  />
                </div>
                <div className="form-group">
                    <label>District</label>
                    <input type="text" className="form-control" name="district" value={district} />
                </div>
                <div className="form-group">
                    <label>State</label>
                    <input type="text" className="form-control" name="state" value={statename} />
                </div>
                
                <div class="form-group">
                    <label>Address</label>
                    <textarea class="form-control" name="address" rows="3" value={address} onChange={e=>onInputChange(e)}></textarea>
                </div>
                <div class="form-group">
                    <label>Date Of Birth</label>
                    <DatePicker class="form-control" selected={Date.parse(dob)} onChange={e => setdob(e)} />
                </div>
                <div class="form-group">
                    <label>Date Of Address</label>
                    <DatePicker class="form-control" selected={Date.parse(doa)} onChange={e => setdoa(e)} />
                </div>
                <div className="form-group">
                    <label>Pan Card</label>
                    <input type="text" className="form-control" name="pan" onChange={e=>onInputChange(e)} value={pan} />
                </div>
                <div className="form-group">
                    <label>GST IN</label>
                    <input type="text" className="form-control" name="gstin" onChange={e=>onInputChange(e)} value={gstin} />
                </div>
                <div className="form-group">
                    <label>GST Register Name</label>
                    <input type="text" className="form-control" name="gstname" onChange={e=>onInputChange(e)} value={gstname} />
                </div>
                <button type="submit" class="btn btn-primary">Update</button>       
            </form>
        </div>
    )
}

export default Customeredit;