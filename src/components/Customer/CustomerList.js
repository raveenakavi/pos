import React, { useEffect, useState } from 'react';
import {Link} from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Store from "@material-ui/icons/Store";
import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Accessibility from "@material-ui/icons/Accessibility";
import BugReport from "@material-ui/icons/BugReport";
import Code from "@material-ui/icons/Code";
import Cloud from "@material-ui/icons/Cloud";
// core common
import GridItem from "common/Grid/GridItem.js";
import GridContainer from "common/Grid/GridContainer.js";
import Table from "common/Table/Table.js";
import Tasks from "common/Tasks/Tasks.js";
import CustomTabs from "common/CustomTabs/CustomTabs.js";
import Danger from "common/Typography/Danger.js";
import Card from "common/Card/Card.js";
import CardHeader from "common/Card/CardHeader.js";
import CardIcon from "common/Card/CardIcon.js";
import CardBody from "common/Card/CardBody.js";
import CardFooter from "common/Card/CardFooter.js";
import axios from 'axios';
import Checkbox from '@material-ui/core/Checkbox';

import { bugs, website, server } from "variables/general.js";
import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";
import { Button } from "@material-ui/core";

const useStyles = makeStyles(styles);

export default function Dashboard() {
  useEffect(()=>{
    registerlist();
},[]);

const [customerdata , setcustomerdata] = useState([]);
const registerlist = async ()=>{
    await axios.get('http://localhost:5001/customer').then((res)=>{
        setcustomerdata(res.data);
    }).catch((err)=>{
        setcustomerdata([]);
    });
}
const deletecustomer = async(id)=>{ 
  await axios.delete(`http://localhost:5001/customer/delete/${id}`);
  registerlist();
}
const [checked, setChecked] = React.useState(true);

  const handleChange = (event) => {
    setChecked(event.target.checked);
  };

  const classes = useStyles();
  return (
    <div>
      <GridContainer>

        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="warning">
              <div class="left"><h4 className={classes.cardTitleWhite}>Customer List</h4></div>
              <div class="right"><Button  className={classes.cardTitleWhite} href="/admin/user">Add</Button></div>

              <p className={classes.cardCategoryWhite}>
                
              </p>
            </CardHeader>
            <CardBody>
            <table class="table">
                    <thead class="thead-dark">
                        <tr>
                          <th><Checkbox
        defaultChecked
        color="primary"
        inputProps={{ 'aria-label': 'secondary checkbox' }}
      /></th>
                            <th scope="col" xs={2} sm={2} md={2}>id</th>
                            <th scope="col" xs={2} sm={2} md={2}>Username</th>
                            <th scope="col" xs={2} sm={2} md={2}>email</th>
                            <th scope="col" xs={2} sm={2} md={2}>Contact Number</th>
                            <th scope="col"xs={2} sm={2} md={2} colSpan={2}>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            customerdata.map((value,index)=>{
                                return <tr>
                                  <td><Checkbox
        defaultChecked
        color="primary"
        inputProps={{ 'aria-label': 'secondary checkbox' }}
      /></td><td scope="row">{index+1}</td>
                                            <td>{value.username}</td>
                                            <td>{value.email}</td>
                                            <td>{value.contact_number}</td>
                                            <td><Link class="btn btn-primary float-left" to={`/Customer/Edit/${value._id}`} ><IconButton class="btn btn-primary float-left" aria-label="edit" disabled color="primary" onClick={`/customer/edit/${value._id}`}>        <EditIcon />
      </IconButton></Link></td>
                                            {/* <Link class="btn btn-primary float-left" >Delete</Link> */}
                                            <td><Link class="btn btn-primary float-left" onClick={(e)=>deletecustomer(value._id)}><IconButton class="btn btn-primary float-left" aria-label="delete" disabled color="primary" >
        <DeleteIcon />
      </IconButton>
      </Link></td>
                                            </tr>
                            })
                        }
                        {/* <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        </tr> */}
                        
                    </tbody>
                </table>
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    </div>
  );
}
