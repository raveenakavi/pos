import React from "react";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import Button from "common/CustomButtons/Button.js";

import Icon from "@material-ui/core/Icon";
// @material-ui/icons
import Store from "@material-ui/icons/Store";
import Warning from "@material-ui/icons/Warning";
import DateRange from "@material-ui/icons/DateRange";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Update from "@material-ui/icons/Update";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import AccessTime from "@material-ui/icons/AccessTime";
import Accessibility from "@material-ui/icons/Accessibility";
import BugReport from "@material-ui/icons/BugReport";
import Code from "@material-ui/icons/Code";
import Cloud from "@material-ui/icons/Cloud";
// core common
import GridItem from "common/Grid/GridItem.js";
import GridContainer from "common/Grid/GridContainer.js";
import Table from "common/Table/Table.js";
import Tasks from "common/Tasks/Tasks.js";
import CustomTabs from "common/CustomTabs/CustomTabs.js";
import Danger from "common/Typography/Danger.js";
import Card from "common/Card/Card.js";
import CardHeader from "common/Card/CardHeader.js";
import CardIcon from "common/Card/CardIcon.js";
import CardBody from "common/Card/CardBody.js";
import CardFooter from "common/Card/CardFooter.js";

import { bugs, website, server } from "variables/general.js";
import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";

const useStyles = makeStyles(styles);

export default function Dashboard() {
  const classes = useStyles();
  return (
    <div>
          <GridContainer justify="center">
      <GridItem xs={12} sm={12} md={8}>
        <Card>
          <CardHeader color="info">
            <h4 className={classes.cardTitleWhite}>
              Customer Identification
            </h4>
          </CardHeader>
          <CardBody>
            <div className={classes.tableUpgradeWrapper}>
              <table className={classes.table}>
                <thead>
                  <tr>
                    <td>Membership Number</td>
                    <th className={classes.center}>111111</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Contact Number</td>
                    <td className={classes.center}>1234567890</td>
                  </tr>
                  <tr>
                    <td>Customer Name</td>
                    <td className={classes.center}></td>
                  </tr>
                  <tr>
                    <td>Address</td>
                    <td className={classes.center}>---</td>
                  </tr>
                  <tr>
                    <td>District</td>
                    <td className={classes.center}>---</td>
                  </tr>
                  <tr>
                    <td>State</td>
                    <td className={classes.center}>---</td>
                  </tr>
                  <tr>
                    <td>Pin</td>
                    <td className={classes.center}>---</td>
                  </tr>
                  <tr>
                    <td >Available Points</td>
                    <td className={classes.center}>
                      <Button round disabled>
                        1000
                      </Button>
                    </td>
                    <td className={classes.center}>
                      <Button
                        round
                        color="danger"
                        href="#"
                      >
                        Redeem
                      </Button> *
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <CustomTabs
            headerColor="primary"
            tabs={[
              {
                tabName: "Available Vouchers",
                tabIcon: BugReport,
                tabContent: (
                  <Tasks
                    checkedIndexes={[0, 6]}
                    tasksIndexes={[0, 1, 2, 3, 4, 5, 6]}
                    tasks={bugs}
                  />
                )
              },
            ]}
          />
        </GridItem>
      </GridContainer>

{/* 
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <CustomTabs
            title="Tasks:"
            headerColor="primary"
            tabs={[
              {
                tabName: "Bugs",
                tabIcon: BugReport,
                tabContent: (
                  <Tasks
                    checkedIndexes={[0, 3]}
                    tasksIndexes={[0, 1, 2, 3]}
                    tasks={bugs}
                  />
                )
              },
              {
                tabName: "Website",
                tabIcon: Code,
                tabContent: (
                  <Tasks
                    checkedIndexes={[0]}
                    tasksIndexes={[0, 1]}
                    tasks={website}
                  />
                )
              },
              {
                tabName: "Server",
                tabIcon: Cloud,
                tabContent: (
                  <Tasks
                    checkedIndexes={[1]}
                    tasksIndexes={[0, 1, 2]}
                    tasks={server}
                  />
                )
              }
            ]}
          />
        </GridItem>
      </GridContainer> */}
    </div>
  );
}
