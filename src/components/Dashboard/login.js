import React, { useState } from 'react';
import axios from 'axios';
import GridItem from "common/Grid/GridItem.js";
import GridContainer from "common/Grid/GridContainer.js";
import Card from "common/Card/Card.js";
import CardHeader from "common/Card/CardHeader.js";
import CardAvatar from "common/Card/CardAvatar.js";
import CardBody from "common/Card/CardBody.js";
import CardFooter from "common/Card/CardFooter.js";
import Button from "common/CustomButtons/Button.js";
import Primary from 'common/Typography/Primary';



const Login = (props)=>{
   const [email,setemail] = useState('');
   const [password,setpassword] = useState('');
   
   const handleSubmit = (evt) => {
    evt.preventDefault();
    const data = {email:email,password:password};
    axios.post("http://localhost:5001/login",data).then((res)=>{
        localStorage.setItem('auth',JSON.stringify(res.data));
        props.history.push('/admin/CustomerList'); 
        
    }).catch((err)=>{
        console.log("failed");
    });
   
}

    return(
        <div className="divpad">
            <form onSubmit={handleSubmit}>
            <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card className="containerCenter">
            <CardHeader color="primary"><h2>POS - Login</h2>
            </CardHeader>
            <CardBody>
              <GridContainer>
              <GridItem xs={12} sm={12} md={4}>

                <label><b>Email:</b></label>
                </GridItem>
                <GridItem xs={12} sm={12} md={8}>
                <input type="text" name="email" value={email} onChange={(e)=>setemail(e.target.value)} /><br />
                </GridItem>
                <GridItem xs={12} sm={12} md={4}><br />
                <label><b>Password:</b></label>
                </GridItem>
                <GridItem xs={12} sm={12} md={4}><br />
                <input type="text" name="password" value={password} onChange={(e)=>setpassword(e.target.value)} />
                </GridItem>
                <GridItem xs={12} sm={12} md={12}><br />
                <CardFooter>
                <button color="primary">Submit</button>
                </CardFooter>
                </GridItem>
                </GridContainer>
                </CardBody>
                </Card>
                </GridItem>
                </GridContainer>
            </form>
        </div>
        
    )
}

export default Login;