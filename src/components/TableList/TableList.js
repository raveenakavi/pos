import React from "react";
// @material-ui/core common
import { makeStyles } from "@material-ui/core/styles";
// core common
import GridItem from "common/Grid/GridItem.js";
import GridContainer from "common/Grid/GridContainer.js";
import Table from "common/Table/Table.js";
import Card from "common/Card/Card.js";
import CardHeader from "common/Card/CardHeader.js";
import CardBody from "common/Card/CardBody.js";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "500",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);

export default function TableList() {
  const classes = useStyles();
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>List</h4>
            <p className={classes.cardCategoryWhite}>
             Morethan Rs.10000 get 5% Dis up to Rs.2000 through HDFC Credit Card
            </p>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={["S.No", "IMEI", "Brand", "Product", "Model", "Price with GST", "Offers"]}
              tableData={[
                ["1","Scanning", "Auto", "Auto", "Auto", "Auto", "Hdfc Brand Emi"],
                ["2","", "", "", "", "", ""],
                ["3","", "", "", "", "", ""],
                ["4","", "", "", "", "", ""],
                ["5","", "", "", "", "", ""],
                ["","", "", "", "Total", "", "More than Rs.10,000"],
              ]}
            />
          </CardBody>
        </Card>
      </GridItem>
      <GridItem xs={12} sm={12} md={12}>
        <Card plain>
          <CardHeader plain color="primary">
            <h4 className={classes.cardTitleWhite}>
              Related Products to be shown by line item wise as notification Box
            </h4>
          </CardHeader>
          {/* <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={["ID", "Name", "Country", "City", "Salary"]}
              tableData={[
                ["1", "Dakota Rice", "$36,738", "Niger", "Oud-Turnhout"],
                ["2", "Minerva Hooper", "$23,789", "Curaçao", "Sinaai-Waas"],
                ["3", "Sage Rodriguez", "$56,142", "Netherlands", "Baileux"],
                [
                  "4",
                  "Philip Chaney",
                  "$38,735",
                  "Korea, South",
                  "Overland Park"
                ],
                [
                  "5",
                  "Doris Greene",
                  "$63,542",
                  "Malawi",
                  "Feldkirchen in Kärnten"
                ],
                ["6", "Mason Porter", "$78,615", "Chile", "Gloucester"]
              ]}
            />
          </CardBody> */}
        </Card>
      </GridItem>
    </GridContainer>
  );
}
