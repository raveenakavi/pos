import React from "react";
// @material-ui/core common
import { makeStyles } from "@material-ui/core/styles";
// core common
import GridItem from "common/Grid/GridItem.js";
import GridContainer from "common/Grid/GridContainer.js";
import Table from "common/Table/Table.js";
import Card from "common/Card/Card.js";
import CardHeader from "common/Card/CardHeader.js";
import CardBody from "common/Card/CardBody.js";

const styles = {
  typo: {
    paddingLeft: "25%",
    marginBottom: "40px",
    position: "relative"
  },
  note: {
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    bottom: "10px",
    color: "#c0c1c2",
    display: "block",
    fontWeight: "400",
    fontSize: "13px",
    lineHeight: "13px",
    left: "0",
    marginLeft: "20px",
    position: "absolute",
    width: "260px"
  },
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "500",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

const useStyles = makeStyles(styles);

export default function TypographyPage() {
  const classes = useStyles();
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>List</h4>
            <p className={classes.cardCategoryWhite}>
             Morethan Rs.10000 get 5% Dis up to Rs.2000 through HDFC Credit Card
            </p>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={["S.No", "MOP", "Auto", "Amount", "Card Details"]}
              tableData={[
                ["1","Cash", "Auto", "2000", "NA"],
                ["2","", "", "", ""],
                ["3","", "", "", ""],
                ["4","", "", "", ""],
                ["5","", "", "", ""],
                ["","", "Total", "", ""],
              ]}
            />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  );
}
